<?php

/**
 * @file
 * The include file of the calculator module.
 */

/**
 * Implements hook_form().
 */
function calculator_form($form, &$form_state) {
  $form = array();

  $form['textfield'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#size' => 25,
  );

  // Create the button.
  // Add the markup, class and name of the button.
  $buttons = array(
    1 => array(
      'markup' => '1',
      'class' => 'button first',
      'name' => 'button_1',
    ),
    2 => array(
      'markup' => '2',
      'class' => 'button',
      'name' => 'button_2',
    ),
    3 => array(
      'markup' => '3',
      'class' => 'button',
      'name' => 'button_3',
    ),
    4 => array(
      'markup' => '+',
      'class' => 'button last',
      'name' => 'button_plus',
    ),
    5 => array(
      'markup' => '4',
      'class' => 'button first',
      'name' => 'button_4',
    ),
    6 => array(
      'markup' => '5',
      'class' => 'button',
      'name' => 'button_5',
    ),
    7 => array(
      'markup' => '6',
      'class' => 'button',
      'name' => 'button_6',
    ),
    8 => array(
      'markup' => '-',
      'class' => 'button last',
      'name' => 'button_min',
    ),
    9 => array(
      'markup' => '7',
      'class' => 'button first',
      'name' => 'button_7',
    ),
    10 => array(
      'markup' => '8',
      'class' => 'button',
      'name' => 'button_8',
    ),
    11 => array(
      'markup' => '9',
      'class' => 'button',
      'name' => 'button_9',
    ),
    12 => array(
      'markup' => '*',
      'class' => 'button last',
      'name' => 'button_times',
    ),
    13 => array(
      'markup' => '(',
      'class' => 'button first',
      'name' => 'button_open',
    ),
    14 => array(
      'markup' => '0',
      'class' => 'button',
      'name' => 'button_0',
    ),
    15 => array(
      'markup' => ')',
      'class' => 'button',
      'name' => 'button_close',
    ),
    16 => array(
      'markup' => '/',
      'class' => 'button last',
      'name' => 'button_devide',
    ),
    17 => array(
      'markup' => '=',
      'class' => 'calculatebutton',
      'name' => 'calculate',
    ),
    18 => array(
      'markup' => '.',
      'class' => 'button',
      'name' => 'button_point',
    ),
    19 => array(
      'markup' => 'c',
      'class' => 'button last',
      'name' => 'button_clear',
    ),
  );

  // Loop trough the options and add the button.
  foreach ($buttons as $button) {
    $form[$button['name']] = array(
      '#prefix' => '<div class="' . $button['class'] . '" >',
      '#suffix' => '</div>',
      '#type' => 'markup',
      '#markup' => $button['markup'],
    );
  }

  $path = drupal_get_path('module', 'calculator');
  // Attach the CSS and JS to the form.
  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => $path . '/css/calculator.css',
    ),
    'js' => array(
      'type' => 'file',
      'data' => $path . '/js/calculator.js',
    ),
  );

  return $form;
}

/**
 * Entrance function for ajax.
 */
function calculator_js_bereken() {
  $data = calculator_calculate_string($_POST['value']);

  drupal_json_output(array('data' => $data));
}

/**
 * This is the function which is responible for the calculation.
 *
 * @param string $mathstring
 *   The string to calculate with.
 *
 * @return string
 *   The actual result of the calculation.
 */
function calculator_calculate_string($mathstring) {
  // Trim whitespaces.
  $mathstring = trim($mathstring);
  // Remove any non-numbers chars; exception for math operators.
  $mathstring_replaced = preg_replace('[^0-9\+-\*\/\(\) ]', '', $mathstring);

  // Do the magix.
  $compute = create_function("", "return ( " . $mathstring_replaced . ");");
  if ($compute) {
    return 0 + $compute();
  }
  else {
    // The calculation failed.
    // Send a error.
    return t("Error");
  }
}
