General description

Calculator is a very simple calculator module.
Add the calculator-block to a region and enjoy it.

The real match is done on the server-side. The browser interacts with the server
with ajax. After the math is done, the result is send back to the browser in a
json-package.

Installation

Copy the folder calculator to your module folder and then enable it on the
admin module page. Add the block to a desired block-region on the blocks page.

future plans

- Configure the color of the characters and buttons.
