<?php
/**
 * @file
 * This is the template for the calculator
 * @var array $variables
 */
?>
<div id = "calculator">
  <div id = "claculator_text" class = "calc_text">
    <div id = "calc_loading">
      &nbsp;
    </div>
    <?php print drupal_render($variables['form']['textfield']);
    ?>
  </div>
  <div class="calcrow">
    <?php print drupal_render($variables['form']['button_1']); ?>
    <?php print drupal_render($variables['form']['button_2']); ?>
    <?php print drupal_render($variables['form']['button_3']); ?>
    <?php print drupal_render($variables['form']['button_plus']); ?>
  </div>
  <div class="calcrow">
    <?php print drupal_render($variables['form']['button_4']); ?>
    <?php print drupal_render($variables['form']['button_5']); ?>
    <?php print drupal_render($variables['form']['button_6']); ?>
    <?php print drupal_render($variables['form']['button_min']); ?>
  </div>
  <div class="calcrow">
    <?php print drupal_render($variables['form']['button_7']); ?>
    <?php print drupal_render($variables['form']['button_8']); ?>
    <?php print drupal_render($variables['form']['button_9']); ?>
    <?php print drupal_render($variables['form']['button_times']); ?>
  </div>
  <div class="calcrow">
    <?php print drupal_render($variables['form']['button_open']); ?>
    <?php print drupal_render($variables['form']['button_0']); ?>
    <?php print drupal_render($variables['form']['button_close']); ?>
    <?php print drupal_render($variables['form']['button_devide']); ?>
  </div>
  <div class="calcrow">
    <?php print drupal_render($variables['form']['calculate']); ?>
    <?php print drupal_render($variables['form']['button_point']); ?>
    <?php print drupal_render($variables['form']['button_clear']); ?>
  </div>
  <br class="clear" />
</div>
<?php print drupal_render_children($variables['form']); ?>
