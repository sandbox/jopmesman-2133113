(function($) {

    $(document).ready(function() {

        // Which button is clicked?
        $('.button').click(function() {
            var value = $('#edit-textfield').val();
            var buttontext = this.textContent;
            if (buttontext != 'c') {
                value = value + buttontext;
                $('#edit-textfield').val(value);
            }
            else {
                $('#edit-textfield').val('');
                $('#calc_loading').hide();
            }
        });

        // The calculator button is clicked.
        $('.calculatebutton').click(function() {
            var value = $('#edit-textfield').val();
            //regel de afhandeling van de berekening
            calculator_calculate(value);
        });

    });

    // The real calculate function.
    function calculator_calculate(value) {
        var path = Drupal.settings.basePath;
        path += 'calculator/js';

        $('#calc_loading').show();

        // Send the textfield to the server.
        var response = $.ajax({
            url: path,
            dataType: "json",
            type: 'POST',
            data: {"value": value},
            cache: false,
            success: (function(data) {
                $('#edit-textfield').val(data.data);
                // On succes fill in the result.
                $('#calc_loading').hide();
            }),
            error: (function(data) {
                $('#edit-textfield').val('error');
                $('#calc_loading').hide();
            })
        })
    }

}(jQuery));
